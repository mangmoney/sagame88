function login_submit()
{

    toastr["info"]("กรุณารอสักครู่")    
    var username = jQuery("#username_val").val()
    var password = jQuery("#password_val").val()
    var csrf_999 = jQuery("#csrf_999").val()

    jQuery("#username_val").attr('disabled', true)
    jQuery("#password_val").attr('disabled', true)
    post_url
    $.post( post_url, { username: username, password: password, csrf_999: csrf_999 }, 'json').done(function(msg){  
        toastr["success"]("ระบบกำลังพาไปหน้าเกม")
        // window.location.replace(redirect_url);
        // setTimeout(() => {
            window.location.replace(redirect_url)
        // }, 000);
    })
    .fail(function(xhr, status, error) {
        console.log(xhr)
        toastr["error"]("Username หรือ Password ผิดพลาด โปรดตรวจสอบข้อมูล")
            jQuery("#username_val").attr('disabled', false)
            jQuery("#password_val").attr('disabled', false)
        // error handling
    });
       
    
}
$(function() {
    $('#password_val').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            login_submit();
        }
    });   

    $('#username_val').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            login_submit();
        }
    });   

    $('#page_username_val').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            page_login_submit();
        }
    }); 

    $('#page_password_val').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            page_login_submit();
        }
    }); 
});

function page_login_submit()
{

    toastr["info"]("กรุณารอสักครู่")    
    var username = jQuery("#page_username_val").val()
    var password = jQuery("#page_password_val").val()
    var csrf_999 = jQuery("#csrf_999").val()

    jQuery("#username_val").attr('disabled', true)
    jQuery("#password_val").attr('disabled', true)
    post_url
    $.post( post_url, { username: username, password: password, csrf_999: csrf_999 }, 'json').done(function(msg){  
        toastr["success"]("ระบบกำลังพาไปหน้าเกม")
        // window.location.replace(redirect_url);
        // setTimeout(() => {
            window.location.replace(redirect_url)
        // }, 000);
    })
    .fail(function(xhr, status, error) {
        toastr["error"]("Username หรือ Password ผิดพลาด โปรดตรวจสอบข้อมูล")
            jQuery("#page_username_val").attr('disabled', false)
            jQuery("#page_password_val").attr('disabled', false)
        // error handling
    });
       
    
}


function detectmob() { 
    if( navigator.userAgent.match(/Android/i)
    || navigator.userAgent.match(/webOS/i)
    || navigator.userAgent.match(/iPhone/i)
    || navigator.userAgent.match(/iPad/i)
    || navigator.userAgent.match(/iPod/i)
    || navigator.userAgent.match(/BlackBerry/i)
    || navigator.userAgent.match(/Windows Phone/i)
    ){
       return true;
     }
   
     
    return false;
}      